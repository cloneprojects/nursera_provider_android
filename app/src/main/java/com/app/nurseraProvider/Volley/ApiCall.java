package com.app.nurseraProvider.Volley;

import android.content.Context;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.nurseraProvider.R;
import com.app.nurseraProvider.application.UberdooXP;
import com.app.nurseraProvider.helpers.AppSettings;
import com.app.nurseraProvider.helpers.Callback;
import com.app.nurseraProvider.helpers.SharedHelper;
import com.app.nurseraProvider.helpers.UrlHelper;
import com.app.nurseraProvider.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

/**
 * Created by user on 23-10-2017.
 */

public class ApiCall {
    private static final int MY_SOCKET_TIMEOUT_MS = 50000;


    public static String strAdd;
    private static String TAG = ApiCall.class.getSimpleName();

    public static void getMethod(final Context context, final String url, final VolleyCallback volleyCallback) {

        Utils.show(context);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(TAG, "url:" + url + ", onResponse: " + response);
                        Utils.dismiss();

                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                if (response.has("error_message")) {
                                    Utils.toast(context, response.optString("error_message"));
                                } else if (response.has("message")) {

                                    Utils.toast(context, response.optString("message"));

                                }
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "url:" + url + ", onErrorResponse: " + error);
                Utils.dismiss();

                VolleyErrorHandler.handle(url, error);


            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        UberdooXP.getInstance().addToRequestQueue(jsonObjReq);

    }


    public static void getMethodHeaders(final Context context, final String url, final VolleyCallback volleyCallback) {

        Utils.show(context);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.dismiss();
                        Log.e(TAG, "url:" + url + ", onResponse: " + response);
                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                if (response.has("error_message")) {
                                    Utils.toast(context, response.optString("error_message"));
                                } else if (response.has("message")) {

                                    Utils.toast(context, response.optString("message"));

                                }
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.dismiss();
                Log.e(TAG, "url:" + url + ", onErrorResponse: " + error);
                VolleyErrorHandler.handle(url, error);


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                AppSettings appSettings = new AppSettings(context);

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + appSettings.getToken());
                return headers;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        UberdooXP.getInstance().addToRequestQueue(jsonObjReq);

    }


    public static void getCompleteAddressString(Context context, double LATITUDE, double LONGITUDE, final Callback callback) {


        String lat = String.valueOf(LATITUDE);
        String lngg = String.valueOf(LONGITUDE);

        final String resultt = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + ","
                + lngg + "&sensor=true&key=" + context.getResources().getString(R.string.google_maps_key);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, resultt, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d(TAG, "onResponse: " + jsonObject);
                Log.d(TAG, "resultUrl: " + resultt);

                callback.onSuccess(jsonObject);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("getjson", "onResponse: " + error);

            }
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        UberdooXP.getInstance().addToRequestQueue(jsonObjectRequest);

    }

    public static void getCompleteAddress(Context context, String LATITUDE, String LONGITUDE, final Callback callback) {


        //String lat = String.valueOf(LATITUDE);
        //String lngg = String.valueOf(LONGITUDE);

        final String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + LATITUDE + ","
                + LONGITUDE + "&sensor=true&key=" + context.getResources().getString(R.string.google_maps_key);
        Log.d(TAG, "url: " + url);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                Log.d(TAG, "onResponse: " + jsonObject);
                callback.onSuccess(jsonObject);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error);
            }
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        UberdooXP.getInstance().addToRequestQueue(jsonObjectRequest);

    }


    public static void PostMethod(final Context context, final String url, JSONObject params, final VolleyCallback volleyCallback) {
        Utils.log(TAG, "url:" + url + ",input: " + params);
        Utils.show(context);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.dismiss();
                        Utils.log(TAG, "url:" + url + ",response: " + response);

                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                if (response.has("error_message")) {
                                    Utils.toast(context, response.optString("error_message"));
                                } else if (response.has("message")) {

                                    Utils.toast(context, response.optString("message"));

                                }
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.dismiss();
                Log.e(TAG, "url:" + url + ", onErrorResponse: " + error);
                VolleyErrorHandler.handle(url, error);
            }
        });
//        jsonObjReq.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 50000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 50000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//
//            }
//        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        UberdooXP.getInstance().addToRequestQueue(jsonObjReq);

    }

    public static void PostMethodWithNoParams(final Context context, final String url, final VolleyCallback volleyCallback) {
        Utils.show(context);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, (JSONObject) null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.dismiss();
                        Log.d(TAG, "onResponse: " + url + ",response:" + response);
                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                Utils.toast(context, response.optString("error_message"));
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.dismiss();
                Log.d(TAG, "onResponse: " + url + ",onErrorResponse:" + error);
                VolleyErrorHandler.handle(url, error);
            }
        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        UberdooXP.getInstance().addToRequestQueue(jsonObjReq);

    }

    public static void PostMethodWithoutProgress(final Context context, final String url, JSONObject params, final VolleyCallback volleyCallback) {
        Utils.log(TAG, "url:" + url + ",input: " + params);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.dismiss();
                        Utils.log(TAG, "url:" + url + ",response: " + response);

                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                if (response.has("error_message")) {
                                    Utils.toast(context, response.optString("error_message"));
                                } else if (response.has("message")) {

                                    Utils.toast(context, response.optString("message"));

                                }
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.dismiss();
                Log.e(TAG, "url:" + url + ", onErrorResponse: " + error);
                VolleyErrorHandler.handle(url, error);
            }
        });

//        jsonObjReq.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 50000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 50000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//
//            }
//        });

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        UberdooXP.getInstance().addToRequestQueue(jsonObjReq);

    }


    public static void PostMethodHeaders(final Context context, final String url, JSONObject params, final VolleyCallback volleyCallback) {
        Utils.show(context);
        Utils.log(TAG, "url:" + url + ",input: " + params);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.dismiss();
                        Log.d(TAG, "onResponse: " + url + ",response:" + response);

                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                if (response.has("error_message")) {
                                    Utils.toast(context, response.optString("error_message"));
                                } else if (response.has("message")) {

                                    Utils.toast(context, response.optString("message"));

                                }
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.dismiss();
                Log.e(TAG, "url:" + url + ", onErrorResponse: " + error);
                VolleyErrorHandler.handle(url, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                AppSettings appSettings = new AppSettings(context);

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + appSettings.getToken());

                return headers;
            }
        };
//        ;
//        jsonObjReq.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 50000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 50000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//
//            }
//        });
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        UberdooXP.getInstance().addToRequestQueue(jsonObjReq);

    }


    public static void PostMethodHeadersNoProgress(final Context context, final String url, JSONObject params, final VolleyCallback volleyCallback) {
        Utils.log(TAG, "url:" + url + ",input: " + params);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Utils.dismiss();
                        Log.d(TAG, "onResponse: " + url + ",response:" + response);
                        if (response.has("error")) {
                            if (response.optString("error").equalsIgnoreCase("false")) {
                                volleyCallback.onSuccess(response);
                            } else {
                                if (response.has("error_message")) {
                                    Utils.toast(context, response.optString("error_message"));
                                } else if (response.has("message")) {

                                    Utils.toast(context, response.optString("message"));

                                }
                            }
                        } else {
                            volleyCallback.onSuccess(response);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Utils.dismiss();
                Log.e(TAG, "url:" + url + ", onErrorResponse: " + error);
                VolleyErrorHandler.handle(url, error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                AppSettings appSettings = new AppSettings(context);

                HashMap<String, String> headers = new HashMap<>();
                headers.put("Accept", "application/json");
                headers.put("Authorization", "Bearer " + appSettings.getToken());
                return headers;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//        jsonObjReq.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 50000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 50000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//
//            }
//        });
        UberdooXP.getInstance().addToRequestQueue(jsonObjReq);

    }

    public static void fileUpload(final Context context, final File document, final VolleyCallback volleyCallback) {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(35, TimeUnit.SECONDS);
        builder.writeTimeout(30, TimeUnit.SECONDS);

        OkHttpClient client = builder.build();
        MediaType FILE_TYPE = MediaType.parse("*/*");

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("document", "fileName",
                        RequestBody.create(FILE_TYPE, new File(document.getPath())))
                .build();

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(UrlHelper.FILE_UPLOAD)
                .post(requestBody).build();

        Log.e(TAG, "url: " + UrlHelper.FILE_UPLOAD);
        Log.e(TAG, "file: " + document.getPath());

       /* client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.e(TAG, "onFailure:IOException " + e);
                Utils.dismiss();
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull okhttp3.Response response) throws IOException {
                if (response.isSuccessful()) {

                    if (response.body() != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            volleyCallback.onSuccess(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.dismiss();
                        }
                    }
                } else {
                    Log.e(TAG, "onFailure: " + response);
                    Utils.dismiss();
                }
            }
        });*/

        okhttp3.Response response = null;
        try {
            response = client.newCall(request).execute();
            Log.e(TAG, "upload_the_image: " + response.message());
            Log.e(TAG, "upload_the_image: " + response.toString());

            JSONObject jsonObject = new JSONObject(response.body().string());
            // Utils.dismiss();
            volleyCallback.onSuccess(jsonObject);

        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e);
        } catch (JSONException e) {
            Log.e(TAG, "JSONException: " + e);
        }
    }

    public static void uploadImage(File image, final Context context, final VolleyCallback volleyCallback) {
        //   Utils.show(context);
//
//        Ion.with(context)
//                .load("POST", UrlHelper.UPLOAD_IMAGE)
//                .setMultipartFile("file", file)
//                .asString()
//                .withResponse()
//                .setCallback(new FutureCallback<com.koushikdutta.ion.Response<String>>() {
//                    @Override
//                    public void onCompleted(Exception e,
//                                            com.koushikdutta.ion.Response<String> result) {
//                        if (e == null) {
//                            try {
//                                JSONObject jsonObject=new JSONObject(result.getResult());
//                                volleyCallback.onSuccess(jsonObject);
//
//                            } catch (JSONException e1) {
//
//                            }
//
//                            Log.d(TAG, "onCompleted: " + result.getResult());
//                        } else {
//                            e.printStackTrace();
//
//                        }
//                    }
//                });
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(30, TimeUnit.SECONDS);
        builder.readTimeout(35, TimeUnit.SECONDS);
        builder.writeTimeout(30, TimeUnit.SECONDS);
        OkHttpClient client = builder.build();
        MediaType MEDIA_TYPE = MediaType.parse("image/jpeg");
        // MediaType FILE_TYPE = MediaType.parse("*/*");

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", "fileName", RequestBody.create(MEDIA_TYPE, new File(image.getPath())))
                // .addFormDataPart("document", "fileName", RequestBody.create(FILE_TYPE, new File(document.getPath())))
                .build();

        okhttp3.Request request = new okhttp3.Request.Builder()
                .url(UrlHelper.UPLOAD_IMAGE)
                .post(requestBody).build();

        Log.e(TAG, "file: " + image.length() / 1024);
        Log.e(TAG, "url: " + UrlHelper.UPLOAD_IMAGE);

       /* client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.e(TAG, "onFailure:IOException " + e);
                Utils.dismiss();
                Toast.makeText(context, R.string.image_failed, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull okhttp3.Response response) throws IOException {

                if (response.isSuccessful()) {
                    Log.e(TAG, "onsuccess: " + response);
                    if (response.body() != null) {

                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            volleyCallback.onSuccess(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Utils.dismiss();
                        }
                    }
                } else {
                    Log.e(TAG, "onFailure: " + response);
                    Utils.dismiss();
                    Toast.makeText(context, R.string.image_failed, Toast.LENGTH_SHORT).show();
                }
            }
        });
*/

        okhttp3.Response response = null;
        try {
            response = client.newCall(request).execute();
            Log.e(TAG, "upload_the_image: " + response.message());

            JSONObject jsonObject = new JSONObject(response.body().string());
            // Utils.dismiss();
            volleyCallback.onSuccess(jsonObject);

        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e);
        } catch (JSONException e) {
            Log.e(TAG, "JSONException: " + e);
        }

    }

}
