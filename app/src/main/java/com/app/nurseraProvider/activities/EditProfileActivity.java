package com.app.nurseraProvider.activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.app.nurseraProvider.R;
import com.app.nurseraProvider.Volley.ApiCall;
import com.app.nurseraProvider.Volley.VolleyCallback;
import com.app.nurseraProvider.helpers.AppSettings;
import com.app.nurseraProvider.helpers.SharedHelper;
import com.app.nurseraProvider.helpers.UrlHelper;
import com.app.nurseraProvider.helpers.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "tag";
    EditText firstName, lastName, mobileNumber, dateOfBirth, aboutYou;
    Spinner genderSpinner;
    Button bottomButton, file_picker;
    ImageView profileImage;
    String[] genders;
    ImageView backButton;
    private Uri uri;
    private File uploadFile;
    private String uploadImagepath;
    private AppSettings appSettings;
    private final int cameraIntent = 2;
    private TextView fileName;
    private static final int READ_REQUEST_CODE = 42;
    private static final int OPEN_DOCUMENT = 119;
    private String filePath = "";
    private File document;
    private TextInputEditText licensing_number;
    private Spinner licensingAuthority;
    private String[] us_states = new String[]{};
    private ArrayList<String> us_states_Arrary = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String theme_value = SharedHelper.getKey(EditProfileActivity.this, "theme_value");
        Utils.SetTheme(EditProfileActivity.this, theme_value);
        setContentView(R.layout.activity_edit_profile);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.BLACK);
        }
        genders = new String[]{getResources().getString(R.string.male), getResources().getString(R.string.female), getResources().getString(R.string.other)};
        initViews();
        initAdapters();
        initListners();
        try {
            setValues();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initListners() {
        bottomButton.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        backButton.setOnClickListener(this);
        file_picker.setOnClickListener(this);
        fileName.setOnClickListener(this);
    }

    private void setValues() throws JSONException {
        JSONObject generalObject = new JSONObject(getIntent().getStringExtra("provider_details"));
        firstName.setText(generalObject.optString("first_name"));
        lastName.setText(generalObject.optString("last_name"));
        mobileNumber.setText(generalObject.optString("mobile"));
        String date = Utils.convertDate(generalObject.optString("dob"));
        dateOfBirth.setText(date);
        aboutYou.setText(generalObject.optString("about"));

        licensing_number.setText(generalObject.optString("licensenumber"));
        // fileName.setText(generalObject.optString("document"));
        licensingAuthority.setSelection(us_states_Arrary.indexOf(generalObject.optString("licensestate")));

        if (generalObject.optString("gender").equalsIgnoreCase("male")) {
            genderSpinner.setSelection(0);
        } else if (generalObject.optString("gender").equalsIgnoreCase("Female")) {
            genderSpinner.setSelection(1);
        } else {
            genderSpinner.setSelection(2);
        }

        uploadImagepath = generalObject.optString("image");
        filePath = generalObject.optString("document");
        Glide.with(this).load(uploadImagepath).placeholder(getResources().getDrawable(R.drawable.profile_pic)).dontAnimate().into(profileImage);

    }

    private void initAdapters() {
        ArrayAdapter aa = new ArrayAdapter(this, R.layout.gender_items, genders);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(aa);

        ArrayAdapter<String> usState = new ArrayAdapter<>(this, R.layout.us_states_items, us_states_Arrary);
        usState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        licensingAuthority.setAdapter(usState);
    }

    private void initViews() {
        appSettings = new AppSettings(EditProfileActivity.this);
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        mobileNumber = (EditText) findViewById(R.id.mobileNumber);
        dateOfBirth = (EditText) findViewById(R.id.dateOfBirth);
        dateOfBirth.setEnabled(false);
        aboutYou = (EditText) findViewById(R.id.aboutYou);
        genderSpinner = (Spinner) findViewById(R.id.genderSpinner);
        profileImage = (ImageView) findViewById(R.id.profileImage);
        Utils.setProfilePicture(EditProfileActivity.this, profileImage);
        bottomButton = (Button) findViewById(R.id.bottomButton);
        Utils.setCustomButton(EditProfileActivity.this, bottomButton);
        backButton = (ImageView) findViewById(R.id.backButton);
        fileName = (TextView) findViewById(R.id.fileName);
        file_picker = (Button) findViewById(R.id.file_picker);
        licensingAuthority = (Spinner) findViewById(R.id.licensingAuthority);
        licensing_number = findViewById(R.id.licensing_number);
        Utils.setTextColour(EditProfileActivity.this, fileName);
        us_states = getResources().getStringArray(R.array.us_states_array);
        us_states_Arrary = new ArrayList<>(Arrays.asList(us_states));
    }


    private void showPictureDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getResources().getString(R.string.choose_your_option));
        String[] items = {getResources().getString(R.string.gallery), getResources().getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
                        } else {
                            choosePhotoFromGallary();
                        }
                        break;
                    case 1:

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
                        } else {
                            takePhotoFromCamera();
                        }


                        break;

                }
            }
        });
        dialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 1);

    }

    private void takePhotoFromCamera() {

        File filepath = Environment.getExternalStorageDirectory();
        final File zoeFolder = new File(filepath.getAbsolutePath(),
                getResources().getString(R.string.app_name)).getAbsoluteFile();
        if (!zoeFolder.exists()) {
            zoeFolder.mkdir();
        }
        File newFolder = new File(zoeFolder,
                getResources().getString(R.string.app_name) + "_Image").getAbsoluteFile();
        if (!newFolder.exists()) {
            newFolder.mkdir();
        }

        Random r = new Random();
        int Low = 1000;
        int High = 10000000;
        int randomImageNo = r.nextInt(High - Low) + Low;
        String camera_captureFile = String.valueOf("PROFILE_IMG_" + randomImageNo);
        final File file = new File(newFolder, camera_captureFile + ".jpg");

//        uri = Uri.fromFile(file);

//        uri = Uri.fromFile(file);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            uri = FileProvider.getUriForFile(EditProfileActivity.this, getPackageName() + ".provider", file);
            Log.d(TAG, "onClick: " + uri.getPath());
            appSettings.setImageUploadPath(file.getAbsolutePath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, cameraIntent);
        } else {
            uri = Uri.fromFile(file);
            appSettings.setImageUploadPath(file.getAbsolutePath());
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivityForResult(takePictureIntent, cameraIntent);
        }
        Log.d(TAG, "onActivityResult: " + appSettings.getImageUploadPath());

        /*Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(i, 2);*/
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("requestCode", "" + requestCode);
        switch (requestCode) {
            case 100:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    takePhotoFromCamera();
                } else {
                    Utils.toast(EditProfileActivity.this, getResources().getString(R.string.camera_permission_error));

                }
                break;

            case 101:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    choosePhotoFromGallary();
                } else {
                    Utils.toast(EditProfileActivity.this, getResources().getString(R.string.storage_permission_error));

                }
                break;
            case OPEN_DOCUMENT:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    performFileSearch();
                } else {
                    Utils.toast(EditProfileActivity.this, getResources().getString(R.string.storage_permission_error));

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case READ_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri;
                    if (data != null) {
                        uri = data.getData();
                        if (uri != null) {
                            Log.d(TAG, "READ_REQUEST_CODE: " + uri);
                            handleFile(uri);
                        } else {
                            Utils.toast(EditProfileActivity.this, getResources().getString(R.string.unable_to_select_file));
                        }
                    }
                }

                break;

            case 1://gallery
                if (data != null) {

                    uri = data.getData();
                    if (uri != null) {
                        handleimage(uri);
                    } else {
                        Utils.toast(EditProfileActivity.this, getResources().getString(R.string.unable_to_select));
                    }
                }
                break;
            case cameraIntent://camera
                uploadFile = new File(String.valueOf(appSettings.getImageUploadPath()));
                if (uploadFile.exists()) {
                    Log.d(TAG, "onActivityResult: " + appSettings.getImageUploadPath());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        profileImage.setColorFilter(ContextCompat.getColor(EditProfileActivity.this, R.color.transparent));
                        Glide.with(EditProfileActivity.this)
                                .load(appSettings.getImageUploadPath())
                                .into(profileImage);
                    } else {
                        profileImage.setColorFilter(getResources().getColor(R.color.transparent));
                        Glide.with(EditProfileActivity.this)
                                .load(appSettings.getImageUploadPath())
                                .into(profileImage);
                    }
                }
                /*if (data != null) {
                    Bundle extras = data.getExtras();
                    Bitmap imageBitmap = (Bitmap) extras.get("data");
                    profileImage.setImageBitmap(imageBitmap);
                    Uri tempUri = Utils.getImageUri(getApplicationContext(), imageBitmap);
                    uploadFile = new File(Utils.getRealPathFromURI(EditProfileActivity.this, tempUri));
                }*/
                break;


        }
    }

    private void handleFile(Uri uri) {

        String file = Utils.getRealPathFromUriNew(EditProfileActivity.this, uri);
        if (file != null) {
            document = new File(file);
            Log.e(TAG, "handleFile: " + document);
            try {
                String filename2 = document.getName();
                Log.e(TAG, "getLastPathSegment: " + filename2);
                fileName.setText(filename2);

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Utils.toast(EditProfileActivity.this, getResources().getString(R.string.unable_to_select_file));
        }
    }

    private void handleimage(Uri uri) {

        profileImage.setColorFilter(getResources().getColor(R.color.transparent));
        Glide.with(EditProfileActivity.this)
                .load(Utils.getRealPathFromUriNew(EditProfileActivity.this, uri))
                .into(profileImage);
        uploadFile = new File(Utils.getRealPathFromURI(EditProfileActivity.this, uri));

    }


    @Override
    public void onClick(View view) {
        if (view == profileImage) {
            showPictureDialog();
        } else if (view == bottomButton) {
            uploadImage();
            /*if (uploadFile == null && document == null) {
                try {
                    Utils.show(EditProfileActivity.this);
                    registerValues();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
//            else if (uploadFile == null) {
//                uploadFile();
//            } else if (document == null) {
//                uploadImage();
//            }
            else {
                uploadImage();
            }*/
        } else if (view == backButton) {
            finish();
        } else if (view == file_picker) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, OPEN_DOCUMENT);
            } else {
                performFileSearch();
            }
        } else if (view == fileName) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, OPEN_DOCUMENT);
            } else {
                performFileSearch();
            }
        }
    }

    public void performFileSearch() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        startActivityForResult(intent, READ_REQUEST_CODE);
    }

    private void uploadImage() {
        if (uploadFile != null) {
            new ImageUpload().execute();
        } else {
            Utils.show(EditProfileActivity.this);
            uploadFile();
        }

    }

    private void uploadFile() {
        if (document != null) {
            new FileUpload().execute();
        } else {

            try {
                registerValues();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class ImageUpload extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Utils.show(EditProfileActivity.this);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ApiCall.uploadImage(uploadFile, EditProfileActivity.this, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {
                    if (response.optString("error_message").equalsIgnoreCase("success")) {
                        uploadImagepath = response.optString("image");
                    } else {
                        Utils.dismiss();
                        Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.unable_to_select_file), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // Utils.dismiss();
            uploadFile();
        }
    }

    private class FileUpload extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Utils.show(EditProfileActivity.this);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            ApiCall.fileUpload(EditProfileActivity.this, document, new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {
                    Log.e(TAG, "response: " + response);
                    if (response.optString("error_message").equalsIgnoreCase("success")) {
                        filePath = response.optString("document");
                    } else {
                        Utils.dismiss();
                        Toast.makeText(EditProfileActivity.this, getResources().getString(R.string.unable_to_select_file), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // Utils.dismiss();
            try {
                registerValues();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    private void registerValues() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("first_name", firstName.getText().toString());
        jsonObject.put("last_name", lastName.getText().toString());
        String date = Utils.convertToServerDate(dateOfBirth.getText().toString());
        Log.e(TAG, "convertToServerDate: " + date);
        jsonObject.put("dob", date);
        jsonObject.put("mobile", mobileNumber.getText().toString());
        jsonObject.put("image", uploadImagepath);
        jsonObject.put("about", aboutYou.getText().toString());
        jsonObject.put("gender", genderSpinner.getSelectedItem().toString());
        jsonObject.put("document", filePath);
        jsonObject.put("licensenumber", licensing_number.getText().toString());
        jsonObject.put("licensestate", licensingAuthority.getSelectedItem().toString());


        ApiCall.PostMethodHeadersNoProgress(EditProfileActivity.this, UrlHelper.UPDATE_PROFILE, jsonObject, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {

                Utils.toast(EditProfileActivity.this, getResources().getString(R.string.profile_updated_successfully));

                moveMainActivity();
            }
        });
    }

    private void moveMainActivity() {
        Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
        intent.putExtra("type", "false");
        startActivity(intent);
    }
}
